# How to Change Language on RNS-300 Navigation System
 
The RNS-300 is a radio navigation system installed in some Volkswagen, Skoda and Seat cars. It has a 6.5-inch monochrome display and a CD player that can read MP3 files. The system supports multiple languages, but changing the language can be tricky if you don't know how to access the hidden menu. Here are the steps to change the language on your RNS-300 navigation system.
 
1. Turn on your RNS-300 system and press and hold the **SETUP** button for about 10 seconds until you see a service menu on the screen.
2. Use the **LEFT** and **RIGHT** buttons to navigate to the **SYSTEM** option and press **OK**.
3. Use the **UP** and **DOWN** buttons to scroll through the available languages and press **OK** to select the one you want.
4. Press and hold the **SETUP** button again to exit the service menu and confirm your language choice.

Congratulations! You have successfully changed the language on your RNS-300 navigation system. Enjoy your drive!
 
**## Download links for files:
[Link 1](https://shoxet.com/2uDdYn)

[Link 2](https://ssurll.com/2uDdYn)

[Link 3](https://shurll.com/2uDdYn)

**



If you want to update the maps on your RNS-300 navigation system, you will need a CD-ROM with the latest map data. You can order one from your dealer or online. Here are the steps to update the maps on your RNS-300 navigation system.

1. Insert the map CD-ROM into the CD slot of your RNS-300 system and wait for it to load.
2. Press the **NAV** button to enter the navigation mode.
3. Press the **SETUP** button and select **MAP UPDATE** from the menu.
4. Follow the on-screen instructions to install the new map data on your system.
5. Eject the map CD-ROM and store it in a safe place.

You have successfully updated the maps on your RNS-300 navigation system. You can now enjoy the latest road information and directions on your screen.

If you want to reset your RNS-300 navigation system to factory settings, you will need to access the hidden service menu. This can be useful if you encounter any problems with your system or want to restore the default settings. Here are the steps to reset your RNS-300 navigation system to factory settings.

1. Turn on your RNS-300 system and press and hold the **SETUP** button for about 10 seconds until you see a service menu on the screen.
2. Use the **LEFT** and **RIGHT** buttons to navigate to the **SYSTEM RESET** option and press **OK**.
3. A confirmation message will appear on the screen. Press **OK** again to proceed with the reset.
4. The system will restart and restore the factory settings. You may need to re-enter your language, time and date preferences.

You have successfully reset your RNS-300 navigation system to factory settings. You can now configure your system according to your preferences.

If you want to pair your phone with your RNS-300 navigation system, you will need to have a Bluetooth interface installed in your car. This can allow you to make and receive calls hands-free and access your phone book on the screen. Here are the steps to pair your phone with your RNS-300 navigation system.

1. Press the **PHONE** button on your RNS-300 system and wait for the message "mobile telephone not inserted or connected" to appear on the screen.
2. Use the **LEFT** and **RIGHT** buttons on the right side of your steering wheel to navigate to the **PHONE** menu on the display between the speedometer and the rev counter.
3. The message "no paired phone found" will appear on the display. Press the **OK** button on the steering wheel to search for a phone.
4. Make sure your phone's Bluetooth is turned on and visible to other devices. Wait as the Bluetooth interface searches for any visible Bluetooth phones.
5. Once your phone is displayed in the device list, select it using the **UP** and **DOWN** buttons on the steering wheel and press **OK**.
6. You will be prompted to enter a password on your phone. The default password is four zeros (0000). Enter the password and confirm it on your phone.
7. Your phone is now paired with the Bluetooth interface. You can create a user profile by pressing **OK**. Your phone book may take some time to upload depending on the number of entries.

You have successfully paired your phone with your RNS-300 navigation system. You can now use your phone hands-free and access your contacts on the screen.
 63edc74c80
 
