# Slavomir Nastasijevic: A Serbian Writer and Historian
 
Slavomir Nastasijevic (1894-1977) was a Serbian writer and historian who wrote several novels and historical studies about medieval Serbia. He is best known for his trilogy of novels about the Serbian despot Stefan Lazarevic (1377-1427), who ruled Serbia after the Battle of Kosovo in 1389. The novels are titled *Tesko Pobedjenima* (Hard to the Conquered), *Vitezi Kneza Lazara* (The Knights of Prince Lazar), and *Despot Stefan* (Despot Stefan).
 
**## Files available for download:
[Link 1](https://tlniurl.com/2uvwRx)

[Link 2](https://cinurl.com/2uvwRx)

[Link 3](https://byltly.com/2uvwRx)

**


 
Nastasijevic's novels are based on extensive research and historical sources, but they also contain fictional elements and poetic language. He portrays the Serbian medieval society and culture with realism and detail, but also with admiration and sympathy. He depicts the heroism and tragedy of the Serbian people who fought against the Ottoman Empire and tried to preserve their independence and identity. He also explores the psychological and moral dilemmas of his characters, who face difficult choices between loyalty, honor, love, and survival.
 
Nastasijevic's novels have been praised by critics and readers as masterpieces of Serbian literature and history. They have been translated into several languages and adapted into films and plays. They are also considered as important sources of inspiration and education for generations of Serbs who value their national heritage and identity.
 
If you are interested in reading Nastasijevic's novels, you can download them in PDF format from various online platforms. However, we recommend that you buy them from reputable publishers or bookstores, as they may offer better quality and support the author's work. Here are some links where you can find Nastasijevic's novels in PDF format:
 
- [Slavomir Nastasijevic - Tesko Pobedjenima](https://www.scribd.com/document/335174339/Slavomir-Nastasijevic-Tesko-Pobedjenima)
- [Slavomir Nastasijevic - Vitezi Kneza Lazara PDF](https://www.scribd.com/document/322181699/Slavomir-Nastasijevic-Vitezi-kneza-Lazara-pdf)
- [Slavomir Nastasijevic - Despot Stefan - PDFSLIDE.NET](https://pdfslide.net/documents/slavomir-nastasijevic-despot-stefan.html)

We hope that you enjoy reading Nastasijevic's novels and learning more about Serbian history and culture.
  
## Slavomir Nastasijevic's Life and Career
 
Slavomir Nastasijevic was born in Gornji Milanovac, a town in central Serbia, in 1904. He came from a family of artists and intellectuals. His father, Nikola Lazarevic, was a builder who moved from Ohrid to Brusnica as a child. His mother, Jelena, was a teacher. His brothers were Zivorad, a painter, Momcilo, a writer, and Svetomir, a composer. Slavomir showed an early interest in literature and history, and he studied classical philology at the Faculty of Philosophy in Belgrade. He also studied history and archaeology at various universities in Europe.
 
Nastasijevic started his literary career as a humorist and playwright. He wrote several comedies that were popular among the audiences and critics. Some of his plays were ZaÄarani kuÄevlasnik (The Bewitched Householder), Å½enidba Pavla Alamunje (The Marriage of Pavle Alamunja), Nestrpljivi naslednici (The Impatient Heirs), VraÄara BoÅ¾ana (The Witch BoÅ¾ana), and NesuÄeni zetovi (The Unintended Sons-in-Law). He also wrote short stories and essays for various magazines and newspapers.
 
After the Second World War, Nastasijevic devoted himself to writing historical novels. He was fascinated by the medieval period of Serbian history, especially by the figure of Stefan Lazarevic, the son of Prince Lazar who ruled Serbia after the Battle of Kosovo. He wrote a trilogy of novels about Stefan Lazarevic's life and reign: Tesko Pobedjenima (Hard to the Conquered), Vitezi Kneza Lazara (The Knights of Prince Lazar), and Despot Stefan (Despot Stefan). He also wrote novels about other historical figures and events, such as Ustanak u Zeti (The Uprising in Zeta), Stefan DuÅ¡an (Stefan DuÅ¡an), Aleksandar Makedonski (Alexander the Great), Julije Cezar (Julius Caesar), Hanibal ante portas (Hannibal at the Gates), and Gvapo (Gvapo).
 
## Slavomir Nastasijevic's Legacy and Influence
 
Slavomir Nastasijevic is regarded as one of the most important Serbian writers and historians of the 20th century. His historical novels are praised for their accuracy, realism, and artistic value. He used historical sources and documents to recreate the historical context and atmosphere of his novels, but he also added fictional elements and poetic language to make them more appealing and engaging. He portrayed the medieval Serbian society and culture with respect and admiration, but he also showed its flaws and contradictions. He depicted the heroism and tragedy of the Serbian people who fought for their freedom and identity against the Ottoman Empire, but he also explored their psychological and moral dilemmas.
 
Slavomir Nastasijevic books pdf free download,  How to download Slavomir Nastasijevic ebooks online,  Slavomir Nastasijevic pdf knjige besplatno skidanje,  Slavomir Nastasijevic best books in pdf format,  Download Slavomir Nastasijevic knjige na srpskom jeziku,  Slavomir Nastasijevic historical novels pdf download,  Slavomir Nastasijevic knjige o srpskoj istoriji pdf,  Where to find Slavomir Nastasijevic books in pdf,  Slavomir Nastasijevic knjige za citanje online pdf,  Slavomir Nastasijevic pdf books download site,  Slavomir Nastasijevic knjige u pdf formatu,  Slavomir Nastasijevic ebooks download pdf free,  Slavomir Nastasijevic knjige pdf download link,  Slavomir Nastasijevic books in english pdf download,  Slavomir Nastasijevic knjige na engleskom jeziku pdf,  Slavomir Nastasijevic pdf knjige download bez registracije,  Slavomir Nastasijevic books for kindle download pdf,  Slavomir Nastasijevic knjige za kindle download pdf,  Slavomir Nastasijevic pdf books torrent download,  Slavomir Nastasijevic knjige torrent download pdf,  Slavomir Nastasijevic books in epub format download pdf,  Slavomir Nastasijevic knjige u epub formatu download pdf,  Slavomir Nastasijevic books review and summary pdf download,  Slavomir Nastasijevic knjige recenzija i sadržaj pdf download,  Slavomir Nastasijevic books list and order pdf download,  Slavomir Nastasijevic knjige spisak i redosled pdf download,  Slavomir Nastasijevic biography and bibliography pdf download,  Slavomir Nastasijevic biografija i bibliografija pdf download,  Slavomir Nastasijevic books quotes and excerpts pdf download,  Slavomir Nastasijevic knjige citati i odlomci pdf download,  Slavomir Nastasijevic books analysis and interpretation pdf download,  Slavomir Nastasijevic knjige analiza i interpretacija pdf download,  Slavomir Nastasijevic books genre and style pdf download,  Slavomir Nastasijevic knjige žanr i stil pdf download,  Slavomir Nastasijevic books themes and motifs pdf download,  Slavomir Nastasijevic knjige teme i motivi pdf download,  Slavomir Nastasijevic books characters and settings pdf download,  Slavomir Nastasijevic knjige likovi i mesta radnje pdf download,  Slavomir Nastasijevic books trivia and facts pdf download,  Slavomir Nastasijevic knjige zanimljivosti i činjenice pdf download,  How to read and enjoy Slavomir Nastasijevic books in pdf format ,  Kako čitati i uživati u Slavomiru Nastašiću knjigama u PDF formatu ,  How to write a book report on Slavomir Nastasijevic books in pdf format ,  Kako napisati izveštaj o knjizi o slavi mira nastasiću knjigama u PDF formatu ,  How to cite and reference Slavomir Nastasijevic books in pdf format ,  Kako citati i navoditi slavi mira nastasiću knjige u PDF formatu ,  How to buy and sell Slavomir Nastasijevic books in pdf format ,  Kako kupiti i prodati slavi mira nastasiću knjige u PDF formatu ,  How to gift and share Slavomir Nastasijevic books in pdf format ,  Kako pokloniti i deliti slavi mira nastasiću knjige u PDF formatu
 
Nastasijevic's novels have influenced generations of Serbs who value their national heritage and identity. They have also contributed to the popularization and education of Serbian history and culture among wider audiences. His novels have been translated into several languages and adapted into films and plays. He has received many awards and honors for his literary work, such as the Order of St. Sava, the Order of St. Cyril and Methodius, the Order of Brotherhood and Unity, the NIN Award, the Zmaj Award, and the Golden Wreath Award.
 
Slavomir Nastasijevic died in Novi Sad in 1983 at the age of 79. He was buried in his hometown of Gornji Milanovac. His legacy lives on through his novels and through his family members who continue to be active in arts and culture.
 63edc74c80
 
